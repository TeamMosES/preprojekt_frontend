export class CostcenterDto{
    id: number;
    cost_id: string;
    description: string;
    category: string;
}
