import { AssignmentDto } from "./assignmentDto";

export class MunicipalDto{
    id: number;
    name: string;
}
