export class HolidayDto{
    id: number;
    begin: Date;
    end: Date;
    reason: String;
}
